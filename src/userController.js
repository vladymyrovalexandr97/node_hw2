import bcrypt from 'bcryptjs';
import userModel from './models/userModel.js';

class Usercontroller {
  async getUser(req, res) {
    return res.json({user: req.user});
  }
  async updateUser(req, res) {
    try {
      const {oldPassword, newPassword} = req.body;
      const user = await userModel.findById(req.user._id);
      if (!user) {
        return res.status(400).json({message: `User not fond`});
      }
      const validPassword = bcrypt.compareSync(oldPassword, user.password);
      if (!validPassword) {
        return res.status(400).json({message: 'Password is not valid'});
      }
      user.password = bcrypt.hashSync(newPassword, 6);
      await user.save();
      return res.json({message: 'Password has been changed'});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Login error'});
    }
  }
  async deleteUser(req, res) {
    try {
      await userModel.findByIdAndDelete(req.user._id);
      res.json({message: 'User has been deleted'});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Login error'});
    }
  }
}

export default new Usercontroller();
