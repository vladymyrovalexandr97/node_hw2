import {Router} from 'express';
import authController from './authController.js';
const authRouter = new Router();


authRouter.post('/register', authController.registration);
authRouter.post('/login', authController.login);

export default authRouter;
