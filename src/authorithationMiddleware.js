import jwt from 'jsonwebtoken';
import userModel from './models/userModel.js';

const authorithationMiddleware = async (req, res, next) => {
  const {authorization} = req.headers;

  if (!authorization) {
    return res.status(400).json({
      message: 'Header authorization not found',
    });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(400).json({
      message: 'Token is empty',
    });
  }

  try {
    const jwtPayload = jwt.verify(token, process.env.SECRET_TOKEN);
    const user = await userModel.findById(jwtPayload.id);

    if (!user) {
      return res.status(400).json({message: 'User profile not found'});
    }

    req.user = user;

    next();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

export default authorithationMiddleware;
