import 'dotenv/config';
import express from 'express';
import mongoose from 'mongoose';
import notesRouter from './src/notesRouter.js';
import authRouter from './src/authRouter.js';
import userRouter from './src/userRouter.js';
import morgan from 'morgan';

const port = process.env.PORT || 8080;
const DB_URL = `mongodb+srv://Svyatogub:TaZaSho@cluster0.ir3xwng.mongodb.net/?retryWrites=true&w=majority`;

const app = express();
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

async function startApp() {
  try {
    await mongoose.connect(DB_URL);
    app.listen(port, () => console.log('server started on port:' + port));
  } catch (e) {
    console.error(`Error on server ${e.message}`);
  }
}
// er handler
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({'message': 'Server error'});
}

startApp();
