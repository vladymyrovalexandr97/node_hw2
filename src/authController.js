import userModel from './models/userModel.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const generateAccessToken = (id, username) => {
  const payload = {
    id,
    username,
  };
  return jwt.sign(payload, process.env.SECRET_TOKEN, {expiresIn: '2h'});
};

/* eslint-disable new-cap */
class authController {
  async registration(req, res) {
    try {
      const {username, password} = req.body;
      const candidate = await userModel.findOne({username});
      if (candidate) {
        res.status(400).json({message: 'User with this name already exists'});
      }
      const hashPassword = bcrypt.hashSync(password, 6);
      const user = new userModel(
          {
            username,
            password: hashPassword,
            createdDate: new Date().toISOString(),
          });
      await user.save();
      return res.json({message: 'User is created'});
    } catch (e) {
      res.status(400).json({message: 'Registration error'});
    }
  }
  async login(req, res) {
    try {
      const {username, password} = req.body;
      const user = await userModel.findOne({username});
      if (!user) {
        res.status(400).json({message: `User: ${username} doesnt exist`});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        res.status(400).json({message: `Incorrect password`});
      }
      const token = generateAccessToken(user._id, user.username);
      return res.status(200).json({
        message: 'Success',
        jwt_token: token,
      });
    } catch (e) {
      res.status(400).json({message: 'Login error'});
    }
  }
}

export default new authController();
