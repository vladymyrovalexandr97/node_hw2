import mongoose from 'mongoose';

const notesSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
});

export default mongoose.model('notes', notesSchema);
