import notesModel from './models/notesModel.js';

/* eslint-disable new-cap */
class Notescontroller {
  async create(req, res) {
    try {
      const note = new notesModel({
        text: req.body.text,
        completed: false,
        userId: req.user._id,
        createdDate: new Date().toISOString(),
      });
      await note.save();
      return res.status(200).json({message: 'Success'});
    } catch (err) {
      return res.status(400).json({message: err.message});
    }
  }
  async getAll(req, res) {
    let {offset, limit} = req.query;

    offset = +offset ||0;
    limit = +limit || 10;

    try {
      const notes = await notesModel.find({userId: req.user._id}, null, {
        limit,
        skip: offset,
      });
      const count = await notesModel.find({userId: req.user._id}).count();

      return res.status(200).json({
        offset,
        limit,
        count,
        notes,
      });
    } catch (err) {
      return res.status(400).json({message: err.message});
    }
  }
  async getOne(req, res) {
    try {
      const {id} = req.params;
      const note = await notesModel.findById(id);
      res.json({note});
    } catch (err) {
      return res.status(400).json({message: err.message});
    }
  }
  async update(req, res) {
    try {
      const {id} = req.params;
      const note = await notesModel.findById(id);
      if (!note) {
        return res.status(400).json({message: 'Note is not found'});
      }
      note.text = req.body.text;
      await note.save();

      res.json({message: 'Success'});
    } catch (err) {
      return res.status(400).json({message: err.message});
    }
  }
  async patch(req, res) {
    try {
      const {id} = req.params;
      const note = await notesModel.findById(id);
      if (!note) {
        return res.status(400).json({message: 'Note is not found'});
      }
      note.completed = !note.completed;
      await note.save();

      res.json({message: 'Success'});
    } catch (err) {
      return res.status(400).json({message: err.message});
    }
  }
  async delete(req, res) {
    try {
      const {id} = req.params;
      const note = await notesModel.findById(id);
      if (!note) {
        return res.status(400).json({message: 'Note is not found'});
      }
      await note.delete();

      res.json({message: 'Success'});
    } catch (err) {
      return res.status(400).json({message: err.message});
    }
  }
}

export default new Notescontroller();
