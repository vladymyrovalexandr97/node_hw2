import {Router} from 'express';
import authorithationMiddleware from './authorithationMiddleware.js';
import notesController from './notesController.js';

const notesRouter = new Router();

notesRouter.post('', authorithationMiddleware, notesController.create);
notesRouter.get('', authorithationMiddleware, notesController.getAll);
notesRouter.get('/:id', authorithationMiddleware, notesController.getOne);
notesRouter.put('/:id', authorithationMiddleware, notesController.update);
notesRouter.patch('/:id', authorithationMiddleware, notesController.patch);
notesRouter.delete('/:id', authorithationMiddleware, notesController.delete);

export default notesRouter;
